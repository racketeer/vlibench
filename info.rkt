#lang info

(define license '(Apache-2.0 OR MIT))

(define version "0.1")

(define deps '("base" "simple-polynomial" "plot" "colorblind-palette" "scribble-math"))

(define pkg-authors '("Dominik Pantůček"))

(define scribblings '(("scribblings/vlibench.scrbl")))
