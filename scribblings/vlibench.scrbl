#lang scribble/manual

@require[scribble-math/dollar
	 racket/future
	 "../private/runinfo.rkt"]

@title[#:style (with-html5 manual-doc-style)]{Variable-Length-Input Benchmarks}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

This package provides a library to measure the running time of given
procedure(s) for successively increasing input sizes and performing
elementary statistical analysis on the resulting series.

@section{Introduction}

This package is currently a work-in-progress. Use with caution!

It is developed for helping with benchmarking and profiling the
@racket[qi] modular compiler.

@section{Statistical Analysis}

@defmodule[vlibench/private/statistics]

A simple module for performing statistical analysis on a set of
results - durations - expected to have normal distribution.

@defstruct*[stats
  ((avg real?)
   (stdev real?)
   (min real?)
   (max real?)
   (count positive-fixnum?))]{

A @racket[struct] representing the result of statistical analysis. The
fields hold @racket[avg] average value, standard deviation
@racket[stdev], minimal sample @racket[min], maximal sample
@racket[max], and the number of samples analysed @racket[count].

}

@defproc[(make-stats (lst (listof real?))) stats?]{

Takes a list @racket[lst] of numbers and performs statistical analysis
on it. It computes the average @${\bar{x}}, standard deviation
@${\sigma(x)}, and minimum and maximum values.

@$${\bar{x}=\frac{1}{n}\sum_{i=1}^n x_i}

@$${\sigma(x)=\sqrt{\frac{1}{n}\sum_{i=1}^n (x_i-\bar{x})^2}}

Returns the @racket[stats?] struct containing the results.

}

@defproc[(statistical-analysis (raw-values (listof real?))
			       (#:max-sigma max-sigma real? 3))
			       (values (listof real?)
			       	       stats?)]{

Performs a more rigorous statistical analysis on given sample of
numbers by repeatedly removing all input values that fall outside
given @racket[max-sigma] range from the average, until all samples are
within the allowed range. This is to make sure the results are not
affected by random glitches that produce extreme values.

Returns two values: the list of remaining samples that are taken into
consideration when calculating the statistical analysis and the actual
statistical analysis results.

}

@section{Benchmarking}

@defmodule[vlibench]

This module provides all procedures for setting the benchmarking up,
running it and formatting results in a scribble document. It
re-provides applicable bindings from the following private modules:

@itemlist[
  @item{@racketmodname[vlibench/profile-defaults] ...}
]

@subsection{Profiles}

@defmodule[vlibench/private/profile-struct]

This module defines a benchmarking profile.

@defstruct*[vlib/profile
  ((name string?)
   (mini fixnum?)
   (mid (or/c fixnum? #f))
   (maxi fixnum?)
   (rep fixnum?)
   (gc (or/c 'never 'step 'always)))]{

Named profile specifying the @racket[mini]mum, @racket[mid]dle, and
@racket[maxi]mum value of input sizes. See
@racket[make-benchmark-steps] for details. In addition it specifies
the number of @racket[rep]eats each input length is benchmarked and
how often the garbage collection should be run based on @racket[gc]
field. Allowed values are: @racket['never] to never run
@racket[collect-garbage] explicitly, @racket['step] to run it after
all the runs for given input size, and @racket['always] to run it
after each measurement.

}

@defproc[(make-vlib-profile (#:name name string?)
			    (#:mini mini fixnum? 10)
			    (#:mid mid fixnum? 10000)
			    (#:maxi maxi fixnum? 1000000)
			    (#:rep rep fixnum?)
			    (#:gc gc (or/c 'never 'step 'always)))
			    vlib/profile?]{

Helper procedure to create @racket[vlib/profile] struct with keyword
arguments specifying the fields and providing reasonable defaults.

}

@subsection{Benchmark Steps / Profile Input Sizes}

@defmodule[vlibench/private/profile-steps]

@defproc[(vlib/profile->steps (profile vlib/profile?))
			      sequence?]{

Returns a @racket[sequence?] of input sizes for given
@racket[profile].

}

@subsection{Benchmark Structs}

@defmodule[vlibench/private/benchmark-structs]

This module contains only the core structs for defininy and running
benchmarks. Also, auxilliary syntaxes are provided to create those
with source syntax included when applicable.

@defstruct*[vlib/prog
  ((label string?)
   (proc (-> any/c any))
   (src syntax?))]{

Single program to be benchmarked with different input sizes. The
@racket[label] field is used to display the results alongside the
source syntax @racket[src] - which can easily be rendered with syntax
highlighting. The procedure @racket[proc] representing the program
must accept single argument and can produce anything.

Usually the procedure accepts a number or list of particular size as
its argument. Such lists are created as part of the @racket[vlib/spec]
specification struct.

}

@defform[(make-vlib/prog label prog)]{

Convenience syntax to populate the @racket[vlib/prog] struct with both
the program and its source alongside specified @racket[label].

}

@defstruct*[vlib/spec
  ((name string?)
   (prepare (-> fixnum? any/c))
   (progs (listof vlib/prog?)))]{

A struct representing a single benchmark specification named
@racket[name], having @racket[prepare] as preparation procedure and
list of @racket[vlib/prog?] in the @racket[progs] field.

The preparation procedure is used to create an input value of desired
length before actual benchmark is being run and measured.

}

@defform[(make-vlib/spec name prepare (label prog) ...)]{

Convenience syntax to create @racket[vlib/spec] of given @racket[name]
and @racket[prepare] preparation procedure containing multiple
programs with given @racket[label]s and their sources.

}

@defstruct*[vlib/result
  ((len fixnum?)
   (raw-times (listof (listof positive-real?)))
   (times (listof (listof positive-real?)))
   (stats (listof stats?)))]{

Single input length result for all progs in given benchmark
specification. The @racket[len] is the input size used,
@racket[raw-times] contains a list of list of all running durations
measured, @racket[times] contains a list of list of running durations
with extremes outside desired max-sigma range removed and stats
contains a list of @racket[stats?] structs with the statistical
analysis results for each program being benchmarked.

}

@defstruct*[vli/benchark
  ((spec vlib/spec?)
   (profile vlib/profile?)
   (results (listof vlib/result))
   (duration positive-real?)
   (fits (listof polynomial?))
   (rfit (list/c real? real?))
   (ratios (listof (listof real?))))]{

Represents complete results of running given benchmark under given
profile. It contains the @racket[spec] specification and
@racket[profile] used a list of @racket[vlib/result?] for all input
sizes, the total @racket[duration] (time spent) @racket[polynomial]
@racket[fits] of each running times of program from the specification
(polynomial of up to 2nd degree), hyperbolic fit (@racket[rfit]) of
the ratio between the first and second program running times and a
matrix (a table) of @racket[ratios] between all programs benchmarked.

}

@subsection{Running Benchmarks}

@defmodule[vlibench/private/benchmark-run]

This module provides the means of running the benchmark specification
under given profile, producing the result.

@defproc[(run-benchmark (spec vlib/spec?)
			(#:profile prof vlib/profile?)) vli/benchmark]{

Runs a benchmark given in specification @racket[spec] under a profile
@racket[prof] and produces @racket[vli/benchmark?] result.

}

@subsection{Benchmark Utils}

@defmodule[vlibench/private/benchmark-utils]

This module provides auxilliary procedures for constructing the
benchmark specifications.

@defproc[(make-random-integer-list (len fixnum?)
				   (max-test-int integer? 1000000))
				   (listof integer?)]{

Creates a random list of integers of given length and maximum
value. Should be used as a prepare field of specifications where the
input is a list of integers.

}

@section{Benchmark Reports}

@defmodule[vlibench/private/report-snippets]

This module contains various scribble snippets allowing for simple
creation of a comprehensive report about given benchmarks.

@defproc[(snippet:vlib/profile (profile vlib/profile?)
			       (heading block? subsection))
			       content?]{

Outputs information about the profile - its name, whether logarithmic
steps are used throughout the tested range or there is a boundary
where the steps change to linear progression, the number of
measurements for each input length and garbage collection settings.

For example:

@nested[#:style 'code-inset]{

@subsubsub*section{Profile: some name}

Logarithmic/Linear Boundary: 10000

Number of measurements for each input length: 100

Garbage collection: never

}

}

@defproc[(snippet:system-information (heading block? subsection)
				     (#:more-versions more-versions list? '())
				     (#:more-generic more-generic list? '()))
				     content?]{

Displays information about the hardware and software of the system
where the benchmarks were run. Racket version, operating system,
architecture, Racket runtime, machine type (CPU class), number of
processor threads and processor descriptive string.

Detailed processor description is not available on Windows platform.

An example output is:

@nested[#:style 'code-inset]{

@subsubsub*section{System Information}

Racket Version: @(racketfont (version)) - @(racketfont (banner))

Operating System: @(racketfont (format "~a" (system-type 'os*)))

Architecture: @(racketfont (format "~a" (system-type 'arch)))

Runtime: @(racketfont (format "~a" (system-type 'vm)))

Machine: @(racketfont (system-type 'machine))

Processor Count: @(racketfont (format "~a" (processor-count)))

Processor: @(racketfont (get-cpu-info-string))

}

}

@defproc[(snippet:hyperbolic-formula (lst (listof real?))) content?]{

Renders hyperbolic equation in the form: @${ax^{-1}+b}. The argument
is a list in the form @racket[(list a b)].

}

@defproc[(snippet:polynomial-formula (fit polynomial?)
				     (max-rank fixnum? 2)
				     (epsilon real? 0.000001))
				     content?]{

Renders polynomial formula of given maximal rank @racket[max-rank] in
the form @${ax^2+bx+c} suitable for inclusion in the alignat* LaTeX
math environment. See @racket[snippet:benchmark-polynomial-formulae]
below.

}

@defproc[(snippet:spec-sources (spec vlib/spec?)) content?]{

Shows a program sources for all programs of given specification
@racket[spec]. For example:

@nested[#:style 'code-inset]{

@subsubsub*section{Program Name:}

@racketblock[
(lambda (lst)
  (for/sum ((v (in-list lst)))
    (* v v)))
]

}

}

@defproc[(snippet:benchmark-polynomial-formulae (benchmark vli/benchmark?)) content?]{

Displays multiple polynomial formulae from @racket[vli/benchmark?]'s
@racket[fits] field aligned using the alignat* LaTeX math
environment. For example:

@nested[#:style 'code-inset]{

@$${
\begin{alignat*}{5}
\text{First Name:}\quad t & = & 2 & x^2 & + & & x & - & 5 \\
\text{Second Name:}\quad t & = & & x^2 & - & 3 & x &  &  \\
\end{alignat*}
}

}

}


@defproc[(snippet:benchmark-performance-matrix (benchmark vli/benchmark?)) content?]{

Display matrix of running time durations ratios comparing all programs
with each other. For example:

@nested[#:style 'code-inset]{

@tabular[
  #:style 'boxed
  #:column-properties '(left right)
  #:row-properties '(bottom-border ())
  (list (list "" "A" "B" "C")
  	(list "A" "1.0" "2.0" "5.0")
	(list "B" "0.5" "1.0" "2.5")
	(list "C" "0.2" "0.4" "1.0"))
]

}

}

@defproc[(snippet:benchmark-ratio-fit (benchmark vli/benchmark?)
				      (nominator-label string? "Q")
				      (denominator-label string? "R"))
				      content?]{

Displays the hyperbolic fit formula for given result. For example:

@nested[#:style 'code-inset]{

@$${\frac{Q}{R}=5x^{-1}+2}

}

}

@defproc[(snippet:benchmar/s-duration (benchmark/s (or/c vli/benchmark?
				      		   	 (listof vli/benchmark?))))
				      content?]{

Displays the duration of single benchmark or a total duration of a
list of benchmarks. The format is "Benchmark(s) took 123 seconds
(00:02:03)."

}

@defproc[(snippet:benchmark-results (benchmark vli/benchmark?)) content?]{

Outputs comprehensive results of given @racket[benchmark] as a
@racket[subsection] labeled with benchmark name. It contains in order:
benchmark duration, programs' sources, graphical plot of running times
for all programs for comparison, polynomial formulae list, performance
matrix, graphical plot of ratios and fitted formula.

}

@defproc[(snippet:summary-results-table (benchmarks (listof vli/benchmark?))) content?]{

Displays summary table of all the benchmark results comparing the
first two implementations for all of them and reporting the ratio of
the first over second.

}
