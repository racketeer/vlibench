#lang racket/base

(require "profile-struct.rkt")

(provide vlib/profiles)

;; Default profiles available with the package.
(define vlib/profiles
  (make-immutable-hash
   `((preview . ,(make-vlib-profile #:name 'preview
                                    #:mid #f
                                    #:rep 20
                                    #:gc 'never))
     (github . ,(make-vlib-profile #:name 'github
                                   #:mid 10000
                                   #:rep 100
                                   #:gc 'step))
     (rigorous . ,(make-vlib-profile #:name 'rigorous
                                     #:mid 10000
                                     #:rep 100
                                     #:gc 'always)))))
